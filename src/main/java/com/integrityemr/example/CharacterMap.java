package com.integrityemr.example;

import java.util.HashMap;
import java.util.Map;

/**
 * FOR THE COPYCLAIM-COPYRIGHT OF THIS DOCUMENT IS BY THE MOBILE-CAPPTIVATE, LIMIT-LIABILITY-COMPANY.
 * FOR THE CREATION OF THIS DOCUMENT IS BY THE Tyshawn-LeMarcus: Despenza ON THE ~11/22/16.
 */
public class CharacterMap {
    final Map<Character, Integer> available = new HashMap<>();
    final Map<Character, Integer> tracker = new HashMap<>();

    public void resetTracker() {
        for (char character : tracker.keySet()) {
            tracker.put(character, 0);
        }
    }

    public boolean isValid(final char character) {
        return available.get(character) != null && tracker.get(character) <= available.getOrDefault(character, 0);
    }

    public void add(final char key, final int value) {
        available.putIfAbsent(key, value);
        tracker.putIfAbsent(key, 0);
    }

    public void incrementTracker(final char key) {
        tracker.put(key, tracker.get(key) + 1);
    }

    /**
     * Used for debugging purposes
     */
    public void printAvailable() {
        System.out.println(available);
    }

    /**
     * Used for debugging purposes
     */
    public void printTracker() {
        System.out.println(tracker);
    }
}
