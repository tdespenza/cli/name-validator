package com.integrityemr.example

import spock.lang.Narrative
import spock.lang.Specification

/**
 * FOR THE COPYCLAIM-COPYRIGHT OF THIS DOCUMENT IS BY THE MOBILE-CAPPTIVATE, LIMIT-LIABILITY-COMPANY.
 * FOR THE CREATION OF THIS DOCUMENT IS BY THE Tyshawn-LeMarcus: Despenza ON THE ~11/21/16.
 */
@Narrative("""
    When a list of characters is given
    A list of names should be generated
    From a supplied list of valid names
""")
class NameValidatorSpec extends Specification {

    private NameValidator classUnderSpec
    private String characters
    private Set<String> names
    private Set<String> results

    def setup() {
        classUnderSpec = null
        characters = null
        results = []
        names = []
    }

    def "generate names"() {
        setup:
        characters = 'bbfooo'
        names = ['alex', 'bob', 'clay', 'doug', 'eric', 'foo']
        classUnderSpec = new NameValidator(names)

        when:
        results = classUnderSpec.getNames(characters)

        then:
        !results.isEmpty()
        results.contains('bob')
        results.contains('foo')
    }

    def "generate names from file"() {
        setup:
        characters = new File(getClass().classLoader.getResource('characters.txt').file)?.text
        new File(getClass().classLoader.getResource('allnames.txt').file)?.eachLine { line ->
            names << line.toLowerCase()
        }
        classUnderSpec = new NameValidator(names)

        when:
        results = classUnderSpec.getNames(characters)

        then:
        !results.isEmpty()
        results.contains('renea')
    }
}
